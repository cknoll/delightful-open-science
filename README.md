# Delightful Open Science [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

Open Science is an umbrella term for a wide range of (proposed) structural changes in the ways science is done. It ranges from Open Access publishing, publishing manuscripts, open peer review and pre-registration of analyses, to open data, open protocols/methods and open source, as well as methods to make data and code Findable, Accessible, Interoperable and Reusable (FAIR). Persistent Identifiers for research objects make such an ecosystem of Open Science tools work. Some include performing science in public and easier ways to collaborate in Open Science. Also involving more people in science is generally included, as well as better methods for the assessment of science, which is the foundation of most problems in science.

This Open Science list is open, just like Open Science itself. What is delightful is rather subjective, because of the background of the initiators the list has started quite nerdy and focussed on infrastructure and scholarly communication. Please help and add more information by adding an "issue" or making a pull request (both options in menu above), especially on topics around reproducibility, meta-science and outreach, where this list is weaker. 

* [Introductions to Open Science](#introductions-to-open-science)
* [News](#news)
    * [News feeds](#feeds)
    * [Podcasts](#podcasts)
    * [Distribution lists](#distribution-lists)
    * [Blogs](#blogs)
    * [Event calendars](#event-calendars)
* [Organizations](#organizations)
    * [General](#general)
    * [Training & community](#training-community)
    * [Diversity & justice](#diversity-justice)
    * [Scientific meetings](#scientific-meetings)
* [Declarations](#declarations)
* [Tools](#tools)
    * [Overviews, lists, databases](#overviews-lists-databases)
    * [General Open Science ecosystem/infrastructure](#general-open-science-ecosystem-infrastructure)
    * [Collaborative research](#collaborative-research)
    * [Open Access/scholarly publishing](#open-access-scholarly-publishing)
        * [Article and manuscript repositories](#article-and-manuscript-repositories)
        * [Publishing systems](#publishing-systems)
        * [Open Peer Review](#open-peer-review)
        * [Various](#various)
    * [Open Data](#open-data)
    * [Persistent identifiers](#persistent-identifiers)
    * [Citations](#citations)


## Introductions to Open Science
* [Open Science MOOC](https://opensciencemooc.eu) - One of the most comprehensive information sources on Open Science with many links to further information.
* [Trainer's materials of FOSTER Open Science](https://www.fosteropenscience.eu/trainers-materials) - Presentations, illustrations and handbooks to teach Open Science. Most with a public domain license.
* [Rainbow of Open Science practices by Bianca Kramer and Jeroen Bosman](https://zenodo.org/record/1147025) - A wide overview of Open Science practices along the entire research work flow. Helpful for picking your next project.
* [Research's Site](https://researcherssite.com) - Accessible introductions/tutorials on better (open) science practices and helpful tools.
* [Open Scientist Handbook](https://openscientist.pubpub.org/pub/play/) - Handbook to help people move science to open.
* [Paywall, The Movie](https://paywallthemovie.com/home) - Can help explain the madness of scientific publishing to outsiders.

### For psychologists & Co.
* [Open Science workshop materials created at LMU Munich](https://osf.io/zjrhu/) - Materials created by psychologists, focused reproducibility and clean statistical analysis.
* [7 Easy Steps to Open Science](https://psyarxiv.com/cfzyx/) - An Annotated Reading List especially for psychologists.

## News
### Feeds
* [Open Science Feed](https://reddit.com/r/Open_Science/) - A daily Open Science news feed. Posts are made on Reddit and syndicated to [Mastodon](https://fediscience.org/@OpenScienceFeed) and [Twitter](https://twitter.com/OpenScienceR). Also present on [Lemmy](https://lemmy.ml/c/openscience).
    * Related Reddit subs are [/r/OpenAccess/](https://reddit.com/r/OpenAccess/), [/r/scihub](https://reddit.com/r/scihub/) and [/r/metaresearch/](https://reddit.com/r/metaresearch/).
* [Open Access Tracking Project](https://cyber.harvard.edu/hoap/Open_Access_Tracking_Project) - is a crowd-sourced very comprehensive information feed managed by Peter Suber. [Available in many versions, on many platforms.](https://cyber.harvard.edu/hoap/OATP_feeds#Versions_of_the_primary_project_feed)
* [BIMS Skolko](http://biomed.news/reports) - Valuable weekly list of articles in scientific journals on scholarly communication. On this page are many BIMS; search for "skolko".

### Podcasts
* [Open Science Radio](http://www.openscienceradio.org/) - A German podcast with Open Science news, often present at conferences. [A large part of the content is in English.](http://www.openscienceradio.org/category/english-episodes/)
* [The Black Goat](https://www.theblackgoatpodcast.com/) - Three psychologists talk about doing science and talk candidly about their experiences.
* [Science for Progress](https://www.scienceforprogress.eu/) - Political podcast highlighting science communicators, science advocates and academic reformers.
* [Open Science TV](https://www.youtube.com/channel/UC1dT_gCSsu7Qkew3W699VkQ) - Radical YouTube channel with interviews and long-form videos on Open Science.

### Distribution lists
* [SCHOLCOMM Discussion List](http://www.ala.org/acrl/issues/scholcomm/scholcommdiscussion) - The association of College and Research Libraries maintains a distribution list on scholarly communication. The name may suggest it focused on outreach, but the focus is publishing.
* [Global Open Access List (GOAL)](http://mailman.ecs.soton.ac.uk/mailman/listinfo/goal) - Discussion list on how to achieve Open Access. Moderated by Richard Poynder.

### Blogs
* [Björn Brembs](http://bjoern.brembs.net) - The blog of neurobiologist Brembs calling for investing the money wasted on publishing into Open Science infrastructure.
* [Generation Research](https://genr.eu/wp/) - Regular news on Open Science, with themes running several weeks.
* [ZBW Blog](https://www.zbw-mediatalk.eu) - Open Science blog of the German National Library of Economics (ZBW). 
* [The scholarly kitchen](https://scholarlykitchen.sspnet.org/) - Blog of the Society for Scholarly Publishing with a large team of chefs making daily posts.

### Event calendars
* [Open Research Calendar](https://openresearchcalendar.github.io/) - Aims to be a global database, but one can notice it started in the UK.
* [ZBW Blog](https://www.zbw-mediatalk.eu/events-calendar/) - Event calendar German National Library of Economics (ZBW) [Open Science Blog](https://www.zbw-mediatalk.eu). Events in English and German.
* [Open Access Community Events](https://oaspa.org/events/) - Events organized by the Open Access Scholarly Publishing Association (OASPA).

## Organizations
### General
* [COAR](https://www.coar-repositories.org/) - Confederation of Open Access Repositories, an international association with all stakeholders.
* [IGDORE](https://igdore.org/) - The Institute for Globally Distributed Open Research and Education is an independent research institute dedicated to improving the quality of science, science education, and quality of life for scientists, students and their families.
* [SciRev](https://scirev.org/ad-information/) -  This system offers researchers the possibility to connect with colleagues across the globe by placing free scientific ads.
* [Invest in Open Infrastructure](https://investinopen.org/) - Initiative that helps people interested in investing on the Open Science Infrastructure to choose were to invest.
* [Radical Open Access Collective](http://radicaloa.disruptivemedia.org.uk/) - A community of scholar-led, not-for-profit presses, journals and other Open Access projects with a progressive vision for open publishing in the humanities and social sciences. They have an interesting [mailing list](https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A0=RADICALOPENACCESS).
* [Stop Tracking Science](https://stoptrackingscience.eu/) - Movement against science tracking that violates fundamental rights and the integrity of an open knowledge society.

### Training & community
* [ReproducibiliTea](https://reproducibilitea.org/) - A group of local Open Science journal clubs discussing papers on reproducibility, Open Science, research quality, or good/bad research practices.
* [RIOT Science Club](http://www.riotscience.co.uk/) - A seminar series that raises awareness and provides training in Reproducible, Interpretable, Open & Transparent science practices. 
* [Open Science Communities](https://inosc-starter-kit.netlify.app/) - A group of local clubs on Open Science, which has started in The Netherlands.
* [The Carpentries](https://carpentries.org/) - A worldwide community of researchers teaching each other foundational coding and data science skills. They come in three flavours: Software, Data and Library Carpentry.
* [CODE4LIB](https://code4lib.org/) - a collective of hackers, designers, architects, curators, catalogers, artists and instigators, who largely work for and with libraries, archives and museums on technology “stuff.”
* [Software Preservation Network](https://www.softwarepreservationnetwork.org/) - A community of practise on the curation and preservation of software.

### Diversity & justice
* [AfricArXiv](https://info.africarxiv.org/) - A community building an African-owned open scholarly repository system for African research.
* [OpenCIDER](https://www.opencider.org/) - A community and resource. The name stands for: Open Computational Inclusion & Digital Equity Resource.
* [Whose Knowledge](https://whoseknowledge.org) - A global campaign to center the knowledge of marginalized communities (the majority of the world) on the internet. 

### Scientific meetings
* [FORCE11](https://www.force11.org/) - FORCE11 is a community of scholars, librarians, archivists, publishers and research funders best known for their workshops with unconference elements.
* [Peer Review Week](https://peerreviewweek.wordpress.com/) - An annual global event celebrating the essential role that peer review plays in maintaining scientific quality.
* [LIBER Europe](https://liberconference.eu/) - Annual conference of [LIBER](https://libereurope.eu/), Europe’s research library association.
* [Open Science Conference](https://www.open-science-conference.eu/) - Annual conference in Germany for researchers, librarians, practitioners, infrastructure providers and policy makers. Especially recommended is the [Open Science Barcamp](https://www.open-science-conference.eu/barcamp/) the day before.
* [Munin Conference](https://site.uit.no/muninconf/) - An annual conference on scholarly publishing under the Northern Lights.
* [Gathering for Open Science Hardware](https://openhardware.science/about/why-gosh/) - GOSH convenes meetings and activities and makes publications on Open Hardware.

## Declarations
* [The 1998 declaration of San Jose](http://red.bvsalud.org/modelo-bvs/wp-content/uploads/sites/3/2016/11/Declaration-of-San-Jose.pdf) - One of the earlier Open Science declarations on a virtual health library.
* [Budapest Declaration](https://www.budapestopenaccessinitiative.org/read) - Budapest Open Access Initiative from 2002 contains one of the most widely used definitions of Open Access.
* [Berlin Declaration](https://web.archive.org/web/20151027030958/http://openaccess.mpg.de/Berlin-Declaration) - Declaration from 2003 on Open Access to Knowledge in the Sciences and Humanities.
* [Declaration on Research Assessment](https://sfdora.org/) - The 2012 San Francisco declaration on scholarly research evaluation. Slightly oversimplified: [Death to the Impact Factor!!](https://www.nature.com/articles/d41586-018-01642-w)
* [Helsinki Initiative](https://www.helsinki-initiative.org/) - Initiative on Multilingualism in Scholarly Communication from 2019.
* [UN Declaration](https://en.unesco.org/news/unesco-who-and-high-commissioner-human-rights-call-open-science) - In 2020 the UNESCO, WHO and the UN High Commissioner for Human Rights call for “Open Science”.

## Tools
### Overviews, lists, databases
* [Rainbow of Open Science practices/tools](https://zenodo.org/record/1147025) - A wide overview, along the entire research work flow, of Open Science practices and tools that help. Made by Bianca Kramer and Jeroen Bosman (University of Utrecht).
* [The varying openness of digital Open Science tools ](https://f1000research.com/articles/9-1292/v1) - Just because of tool is free to use, does not mean it is open and openness comes in degrees. For example, by using GitHub to openly share your code, you exclude researchers from several countries.
* [Commons-compliant tools/platforms worksheet](https://docs.google.com/spreadsheets/d/1h0Aq6NYIeVnLDw33vx1SGnv1jbE2B7widbHhU7tpiUw/edit#gid=2141288902) - This tools helps with the selection of Open Science tools for various stages of the research work flow, depending on which aspects of open you find most important.
* [Scholarly Communication Technology Catalogue (SComCat)](https://www.scomcat.net) - A comprehensive list of Open Science tools around scholarly communication (publishing, discovery, peer review), which you can filter on several dimensions. 

### General Open Science ecosystem/infrastructure
* [OpenAIRE](https://www.openaire.eu/) - A large EU project on Open Science. Possibly best known for their [OpenAIRE Research Graph](https://graph.openaire.eu/), a large linked database of research data items.
* [European Open Science Cloud](https://eosc-portal.eu/) - Europe is not building one large cloud, but working to interconnect science services and resources. The EOSC is at the moment a vision, not yet a thing.
* [Open Science Framework](http://osf.io) - The OSF of the Center for Open Science connects many research resources and promotes collaborative research. They host preprint archives and build a [Preregistration system.](http://osf.io/prereg/)
* [scienceverse](https://scienceverse.github.io/scienceverse/) - scienceverse aims to make every aspect of [hypothesis testing based] research findable, accessible, interoperable and reusable.
* [Vendor Lock-in](https://de.wikipedia.org/wiki/Lock-in-Effekt) - When you work with closed source, closed data vendors they control you.

### Collaborative research
* Writing resources are in this [Awesome List Scientific Writing.](https://github.com/writing-resources/awesome-scientific-writing)
* [NextCloud](https://nextcloud.com/) - NextCloud allows you to host a data cloud yourself, with all the corresponding privacy and freedom benefits. The system includes two office packages similar to Google Docs: [Collabora office](https://nextcloud.com/collaboraonline/) and [OnlyOffice](https://nextcloud.com/onlyoffice/). 
* [Collaborative Scientific MarkDown](https://mur2.co.uk/editor) - MarkDown is a simple text-file based system that includes simple ways to add Markup to your documents. It is ideal for reuse, archiving and combining with code. The linked system is aimed at scientists and includes equations tables, and references.
* [Overleaf](https://www.overleaf.com/) - LaTex based collaborative writing system.
* [Jupyter Notebooks](https://jupyter.org/) - Code your scientific article to have all data and code together and allow others to easily continue where you left off.
* [Zettlr](https://www.zettlr.com/) - Zettlr is a [note taking](https://en.wikipedia.org/wiki/Zettelkasten) and writing tool, it is free open source software, no vendor lock-in ([MarkDown based](https://de.wikipedia.org/wiki/Markdown)), knows references (BibTex, JSON, Zotero), exports to many formats (including Word and HTML).
* [OLKI](https://olki.loria.fr/blog/whatisolki/) - A federated open source resource sharing and communication platform. A [typical use case](https://olki.loria.fr/blog/usecase/) may be the easiest introduction. Work in progress. 
    * [SciFed](https://synalp.frama.io/olki/scifed/) - Draft specification for federating scientific activities using ActivityPub.
* [nLab](https://ncatlab.org/nlab/show/HomePage) - Nice example of collaborative working using a Wiki.
* [BibSonomy](https://www.bibsonomy.org/gettingStarted) - Collaborative reference and bookmark manager.
* [Open Social Media](https://fediscience.org/server-list.html) - Distributed open social media based on Open Science principles: open standards, open code, community based. Linked is a list of academic and research servers/communities.

### Open Access/scholarly publishing
* [The Directory of Open Access Journals](https://doaj.org/) - A comprehensive database with OA journals. Can be searched by field.
* [BASE](https://www.base-search.net/) and [CORE](https://core.ac.uk/) - Search engines for Open Access articles. 
* [Open Knowledge Maps](https://openknowledgemaps.org/) - Find papers (from Pubmed or BASE) via a visual knowledge map.

#### Article and manuscript repositories
* [ArXiv](https://arxiv.org/) - There are nowadays many manuscript servers, from institutional, funding agency to disciplinary repositories, but an honorary mention should go to arXiv, the repository where it all began with physics manuscripts.
* [DOAR](https://v2.sherpa.ac.uk/opendoar/) - The Directory of Open Access Repositories is a large database of manuscript repositories.
* [Unpaywall](https://unpaywall.org/) - If you install their great browser add-on and are on a pay-walled article page, the add-on will point to any free versions of the article.

#### Publishing systems
* [eLife](https://elifesciences.org/) - eLife is an important force when it comes to Open Access publishing and Open Review. It is a publisher, develops software and builds communities.
* [Copernicus](https://publications.copernicus.org/open-access_journals/open_access_journals_a_z.html) - Copernicus is the main Open Review and Open Access innovator in the Earth sciences. It partners with the [European Geosciences Union](https://www.egu.eu/), which has an active Open Science community.
* [SciPost](https://scipost.org/about) - Open Access journals, managed by scientists, open (post-publication) peer review.
* [Open Library of Humanities](https://www.openlibhums.org/site/about/) - The OLH publishes Open Access scholarship without article processing charges (APCs), but funded by an international consortium of libraries.
* [PubPub](https://www.pubpub.org/) - The open-source, privacy-respecting, all-in-one collaborative publishing platform for communities small and large.
* [Open Journal System](https://pkp.sfu.ca/ojs/)  - The Open Journal Systems (OJS) is an open source software application for managing and publishing scholarly journals. Originally developed and released by Public Knowledge Project over 10,000 journals use it worldwide. [PHP, GPL]
* [F1000](https://f1000research.com/) - Faculty of 1000 is an Open Access and Open Peer Review publishing platform supporting data deposition and sharing.
* [Octopus](https://science-octopus.org/) - Legacy publishing demands full scientific papers. In the digital age you can publish parts much sooner. With Octopus you can publish hypotheses, small data sets, methods and peer reviews.
* [Living systematic reviews](https://community.cochrane.org/review-production/production-resources/living-systematic-reviews) - A Cochrane LSR is a systematic review which is continually updated. People and groups outside of medical sciences can do this themselves on a code repository; [see this example](https://github.com/ET-NCMP/et-ncmp.github.io).
* [The Licensing Initiative for Research, Academia, and Science](https://liras.org/) - LIRAS licenses allow researchers to require credit for their work in the form of authorship slots, acknowledgements, or citations.
* [Contributor Roles Taxonomy](http://credit.niso.org/) - CRediT is a high-level taxonomy detailing who contributed what to a paper: Conceptualization, Data curation, Formal Analysis, Funding acquisition, Investigation, Methodology, Project administration, Resources, Software, Supervision, Validation, Visualization, Writing – original draft, Writing – review & editing. [This article explains why this is important.](https://www.nature.com/articles/d41586-019-02084-8)
    * [Tenzing](https://martonbalazskovacs.shinyapps.io/tenzing/) - With this tool you can easily create CRediT in various formats.

#### Open Peer Review
* [ReimagineReview](https://reimaginereview.asapbio.org/) - The largest database with innovative (Open) Peer Review initiatives. 
* [Pubfair](https://www.coar-repositories.org/news-updates/pubfair-version-2-now-available/) - COAR is building a communication system based on ActivityPub that connects Open Access repositories with peer review systems/journals.
* [Publishing peer review materials](https://f1000research.com/articles/7-1655/v1) - An XML standard for representing the peer review reports in Journal Article Tag Suite (JATS) and CrossRef.
* [Segmented peer review](https://www.sciencedirect.com/science/article/pii/S1936523320304770) - A proposal to have reviewers only review the part of an article that is their core expertise. This could also tap into new groups of reviewers: for example, librarians reviewing the search strategies used in systematic review papers.
* [Registered Reports](https://www.nature.com/articles/d41586-019-02674-6) - Register your analysis before the results are in. Some journals review these reports and commit to publishing it independent of outcome. [The Center for Open Science has a system for this.](https://www.cos.io/initiatives/registered-reports) If there is no fitting journal for your study you can still publish your analysis plan on a repository to reduce your [Researcher Degrees of Freedom](https://journals.sagepub.com/doi/full/10.1177/0956797611417632).
* [Registered Replication Reports](https://journals.sagepub.com/doi/10.1177/1745691614543974) - Reproduce important studies with multiple labs and have journals vet the reproduction before the results are in.
* [Peer Community In](https://peercommunityin.org/) - PCI performs open pre-publication peer reviews in a range of life sciences.
* [PubPeer](https://pubpeer.com/) - This is a post-publication peer review system. As it allows for anonymous reviews it is mostly used to point to flaws in scientific papers. To collect peer comments on Twitter it has a [PubPeerBot](https://pubpeer.com/static/faq#31).
* [Self Journal of Science](https://www.sjscience.org/) - A system where every scientist can publish and review articles.
* [Peeriodical](https://peeriodicals.com/) - A lightweight virtual journal with you as the Editor-in-chief. 
* [biOverlay](https://www.bioverlay.org/about/) - Example Overlay journal. These are journals that review and endorse manuscripts published on repositories.
* [OPERAS Certification Service](https://doabooks.org/en/librarians/certification-service) - Certifies peer review processes for Open Access books. If extended to journals this idea could also fight predatory journals.
* [Hypothesis](https://web.hypothes.is) - A tool for web annotations for the scholarly community. This allows you to make (review) comments on any web pages or PDF and share it if you want to. [MIT, BSD]
* [Paperhive](https://paperhive.org/) - Another web annotation tool. Paperhive has more a focus on collaborative reading and education. [[MIT, GPL](https://github.com/paperhive)]
* [Statcheck](https://mbnuijten.com/statcheck/) - Extract statistics from articles and recompute p-values.
* [Plaudit](https://plaudit.pub/) - Open endorsements from the academic community. [[MIT](https://gitlab.com/Flockademic/plaudit/)]
* [Sciety](https://sciety.org/) - Open preprint evaluation and curation together in one place. Navigate the preprint landscape.

#### Various
* [The Initiative for Open Abstracts](https://i4oa.org/) - I4OA is a collaboration to advocate and promote the unrestricted availability of the abstracts.
* [Retraction Watch Database](retractiondatabase.org/RetractionSearch.aspx) - The [blog Retraction Watch](https://retractionwatch.com/) also has a database with all the retracted papers they know.
* [Sherpa Romeo](https://v2.sherpa.ac.uk/romeo/) - Database with journal Open Access policies.
* [Think. Check. Submit.](https://thinkchecksubmit.org/) - This webpage helps researchers identify trusted journals and publishers for their research. Related is [Think. Check. Attend.](https://thinkcheckattend.org/) for conferences.

### Open Data
* [Data Repository Guidance](https://www.nature.com/sdata/policies/repositories) - Nature Magazine helps you find the right repository for your data.
* [Zenodo](https://zenodo.org/) - Repository for (almost?) everything. If there is a more specific repository it tends to be better to use that (to build collections, specific metadata support), but Zenodo is a great backstop. [[GPL](https://github.com/zenodo)] 
* [FAIR data](https://www.go-fair.org/) - FAIR stands for Findability, Accessibility, Interoperability, and Reuse. It is thus orthogonal to Open Data, also closed data can be FAIR, but both concepts are important for building an Open Science infrastructure.
* [Search DataCite](https://search.datacite.org/) - Find datasets with a Digital Object Identifier (DOI).
* [Academic Torrents](https://academictorrents.com/) - Tamperproof system to share large files and articles based on the distributed peer-to-peer system Bittorrent.

### Persistent identifiers
* [CrossRef](https://www.crossref.org/) - A non-profit with publishers as members that gives out the Digital Object Identifiers (DOI) for scientific articles, an important [persistent identifier](https://www.project-freya.eu/) (PID) to connect Open Science tools and make articles easier to find.
* [DataCite](https://datacite.org/) - A group that is traditionally responsible for the DOIs of data, but their support (metadata) for manuscripts and articles is getting more comprehensive. Alternative PIDs are [Handles](https://en.wikipedia.org/wiki/Handle_System) and [ARKs](https://arks.org/).
* [ORCID](https://orcid.org/) - This organization provides PIDs for researchers, which is especially helpful for people changing their names or people from countries with many of the same names.
* [Identifiers.org](https://identifiers.org/) - System to systematically add stable identifiers to collections.
* [PIDapalooza](https://www.pidapalooza.org/) - There are many more PIDs. The PIDapalooza festival celebrates them all. Although we should not be blind to their downside, they are also part of the micro-managing surveillance system.
* [Project FREYA](https://www.project-freya.eu/) - An EU project aiming at building a infrastructure for persistent identifiers (PIDs) as a core component of Open Science.

### Citations
* [WikiCite](http://wikicite.org/) - An initiative to create a bibliographic database based on [Wikidata](https://www.wikidata.org).
* [Zotero](https://www.zotero.org/) - A tool to help you collect, organize, cite, and share research. [Cita](https://www.wikidata.org/wiki/Wikidata:Zotero/Cita) is a Wikidata addon for Zotero that adds citations from Wikidata, and enables users to easily contribute missing data.
* [Scholia](https://scholia.toolforge.org/) - A service that creates visual scholarly profiles using bibliographic and other information in Wikidata.
* [Open Citations](http://opencitations.net/) - An not-for-profit infrastructure organization dedicated to the publication of open bibliographic and citation data. Citation data is important to find newer literature; the largest datasets are pay-walled.
* [Citation Typing Ontology](https://sparontologies.github.io/cito/current/cito.html) - There are many reasons to cite an article, not always positive. This ontology provides a systematic way to indicate the reason. 
* [AnyStyle](https://anystyle.io/) - An API and tool to parses academic references to a machine readable format.
 

## Related collections
* [Awesome List Open Science](https://awesomeopensource.com/projects/open-science)
* [Awesome List Scientific Writing](https://github.com/writing-resources/awesome-scientific-writing)
* [delightful-funding](https://codeberg.org/teaserbot-labs/delightful-funding) - Includes information on funding Open Science.
* [Awesome List Geoscience](https://github.com/softwareunderground/awesome-open-geoscience)
* [Awesome LIst Climate Science](https://github.com/pangeo-data/awesome-open-climate-science)


## Maintainers
If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-open-science/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder) (fediverse: [@humanetech](https://mastodon.social/@humanetech))
- [`@venema`](https://codeberg.org/Venema) (fediverse: [@VictorVenema](https://fediscience.org/@VictorVenema) and [@OpenScienceFeed](https://fediscience.org/@OpenScienceFeed))
- [`@vald_es`](https://codeberg.org/vald_es) (Twitter: [@vald_es](https://twitter.com/Vald_Es))


## Contributors
With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).


## License
[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)